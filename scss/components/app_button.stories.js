export default {title: 'App Button'};

export const small = () => `<a class="ob-app-btn ob-small">
    <div class="ob-btn-icon ob-icon mdi mdi-ferry">
    </div>
    <div class="ob-label">Label</div>
</a>`;
export const smallWithoutLabel = () => `<a class="ob-app-btn ob-small">
    <div class="ob-btn-icon ob-icon mdi mdi-ferry">
    </div>
</a>`;
export const large = () => `<a class="ob-app-btn ob-large">
    <div class="ob-btn-icon ob-icon mdi mdi-ferry">
    </div>
    <div class="ob-label">Label</div>
</a>`;
export const largeWithoutLabel = () => `<a class="ob-app-btn ob-large">
    <div class="ob-btn-icon ob-icon mdi mdi-ferry">
    </div>
</a>`;
