export default {title: 'Toggle button icon'};

export const normal = () => `

<div class="ob-toggle-button-icon" style="width: 192px">
    <div class="ob-toggle-button-icon__container">
         <div class="ob-toggle-button-icon__item">
            <div class="ob-icon"><span class="mdi mdi-account"></span></div>
            <div class="ob-label">Label</div>
         </div>
         <div class="ob-toggle-button-icon__item ob-selected">
            <div class="ob-icon"><span class="mdi mdi-account"></span></div>
            <div class="ob-label">Label</div>
         </div>
         <div class="ob-toggle-button-icon__item">
            <div class="ob-icon"><span class="mdi mdi-account"></span></div>
            <div class="ob-label">Label</div>
         </div>
         <div class="ob-toggle-button-icon__item">
            <div class="ob-icon"><span class="mdi mdi-account"></span></div>
            <div class="ob-label">Label</div>
         </div>
    </div>
</div>`;
